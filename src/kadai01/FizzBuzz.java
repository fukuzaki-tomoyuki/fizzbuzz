package kadai01;

public class FizzBuzz {

	public static void main(String[] args) {
		for(int i = 1; i < 100; ++i) {
			getName(i);
		}
	}

	public static String getName(int i) {
		if (i % 15 == 0) {
			System.out.println("FizzBuzz");
			return "FizzBuzz";
		} else if (i % 3 == 0) {
			System.out.println("Fizz");
			return "Fizz";
		} else if (i % 5 == 0) {
			System.out.println("Buzz");
			return "Buzz";
		} else {
			String s = "" + i;
			System.out.println(s);
			return s;
		}
	}
}
