package jp.co.FizzBuzzTest;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import kadai01.FizzBuzz;

class FizzBuzzTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	public void testGetFizzBuzz() {
		String n1 = FizzBuzz.getName(15);
		assertThat(n1, is("FizzBuzz"));
	}

	@Test
	void testGetFizz() {
		String n1 = FizzBuzz.getName(3);
		assertThat(n1, is("Fizz"));
	}

	@Test
	public void testGetBuzz() {
		String n1 = FizzBuzz.getName(5);
		assertThat(n1, is("Buzz"));
	}
	@Test
	public void testGetNum() {
		String n1 = FizzBuzz.getName(1);
		assertThat(n1, is("1"));
	}
}
